{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MT642A 2022, Exercise 3, Elastic scattering and Diffraction - Teacher's"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Theory and Concepts\n",
    "<img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/scatteringvector.png?inline=false\" alt=\"drawing\" width=\"600\"/>\n",
    "\n",
    "\n",
    "------\n",
    "#### Scattering of wave of particle or oscillating electro-magnetic field\n",
    "Particles, like a neutrons or electrons as well as electromagnetic (EM) radiation, traveling with momentum $\\mathbf{p} = m\\mathbf{v}$ may all be treated as a plane wave with wave vector $\\mathbf{k} = \\hbar \\mathbf{p}$, propagating in space and time so that its amplitude vary according to:\n",
    "$$\\psi(\\mathbf{r}, t) = Ae^{i(\\mathbf{k}\\cdot\\mathbf{r} - \\omega t + \\phi_0)},$$\n",
    "where $\\phi_0$ is the phase offset at origin. The length of the wave vector $|\\mathbf{k}|$ is called the wavenumber $k$ and from the de Broglie relation one has: $k = 2\\pi/\\lambda$.\n",
    "\n",
    "For a single scattering event (inelastic or elastic) the incoming EM field or particle interacts with the scattering point (e.g. an electron through the polarizing effect of an oscillating electric field, or a nucleus via the strong force between neutron and proton). This will result in a change in momentum for the particle (or EM wave) so that the scattered wave have a new wave vector $\\mathbf{k'}$. $\\mathbf{Q} = \\mathbf{k} - \\mathbf{k'}$ is called the scattering vector (if one defines $\\mathbf{Q} = \\mathbf{k'} - \\mathbf{k}$ instead then the scattering vector will just point away from scatter point instead of towards it) and by using the cosine rule in the left figure it is easy to see that the following is true:\n",
    "$$Q^2 = k^2 + k'^2 - 2kk'\\cos(2\\theta).$$\n",
    "For the case of elastic scattering there is no energy transfer and so $k' = k$, and $Q = 2k \\sin(\\theta).$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Bragg's law\n",
    "If the incident plane wave scatters from multiple objects, the amplitudes of the scattered waves will interfere and add to each other. Let's say that there are two scatterers, *i* and *j*.\n",
    "The difference of the phases, $\\Delta\\Phi$, of the scattered waves is equal to $2d\\sin(\\theta)$, where $d$ is the distance between the scattering objects. The waves will therefore interfere constructively only if $2d\\sin(\\theta) = n\\lambda$, where $n \\in \\{1, 2, ...\\}$; this is called the Bragg's law, or the Bragg's condition for constructive interference.\n",
    "\n",
    "As $k = 2\\pi/\\lambda $, one can see that the Bragg's condition is fulfilled if $2d\\sin(\\theta) = 2\\pi n/k \\iff 2k\\sin(\\theta) = 2n\\pi/d \\iff Q = 2n\\pi/d$.\n",
    "\n",
    "This is very close to how we define the reciprocal lattice $\\mathbf{G}$, see below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Laue condition for constructive interference\n",
    "The Bragg's law is very much a combination of trigomentry and superposition principle of waves.\n",
    "More generally, the scattering amplitude is given as a Fourier transform (or convolution) of the unit cell structure factor and Lattice sum: \n",
    "$F^{crystal} = \\sum_j f_j(\\mathbf{Q})e^{i\\mathbf{Q}\\cdot\\mathbf{r_j}} \\sum_n e^{i\\mathbf{Q}\\cdot\\mathbf{R_n}}$, \n",
    "where $\\mathbf{r_j}$ is the position of atom-*j*, inside the unit cell (the basis), $f_j(\\mathbf{Q}) = \\int \\rho(\\mathbf{r})e^{i\\mathbf{Q}\\cdot\\mathbf{r}}d\\mathbf{r}$ are the (x-ray) atomic form factor for atom-*j*, ( scattering lengths $B_j$ for neutrons), $\\mathbf{R_n}$ are the lattice vectors.\n",
    "\n",
    "The Lattice sum is basically just phase-factors ranging between -1 and 1 on the unit circle, and as such the sum will basically be of the order of unity for all $\\mathbf{Q}$ except for those where $\\mathbf{Q}\\cdot\\mathbf{R_n} = 2\\pi m;~ m=\\{1,2,3\\dots\\}~~~~~~ $     (A).\n",
    "\n",
    "Further down we will define the reciprocal lattice, $\\mathbf{G}$ and the nature of that is that the scalar product between $\\mathbf{G}$ and any lattice vector $\\mathbf{R_n}$ is equal to $2\\pi m$ where *m* is an integer. It can then be seen that relationship (A) above is true if and only if $\\mathbf{Q} =\\mathbf{G}$.\n",
    "\n",
    "This is the Laue condition for constructive scattering.\n",
    "\n",
    "Finally the intensity of the experimental peaks is proportional to the squared modulus of the scattering amplitude:\n",
    "\n",
    "$I \\propto |F(Q)|^2 $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Crystal Bravais Lattice\n",
    "In a crystal, objects like atoms, ions, polymers, or whole proteins, occupy symmetric points, or lattice points, situated on, or inside, a 3-dimensional infinite array called a *Bravais* lattice. The Bravais lattice is the collection of points $\\mathbf{x_i}$ in space generated by integer linear combinations of its three *primitive* lattice vectors {$\\mathbf{a}, \\mathbf{b}, \\mathbf{c}$}, through: $\\mathbf{x} = n_1\\mathbf{a} + n_2\\mathbf{b} + n_3\\mathbf{c},\\, {n_1, n_2, n_3} \\in {0, 1, 2,...}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 14 Space groups and 7 Point groups\n",
    "There exist symmetry operations such as *translations, rotations, inversions, and mirror planes* that each one or a combination of will overlap the new lattice on-top of the old lattice. A group of all such combinations for a single Bravais lattice is called a *space group*. For symmetry reasons there are only 14 unique such space groups (and Bravais lattices). A sub-group of a space group is the collection of only those symmetry operations that will leave at least one lattice point unchanged while still take the lattice into itself. This collection is called a *point group*. It turns out that there are 7 unique point groups and they make up the 7 crystal systems or families [Triclinic, Monoclinic, Orthorhombic, Tetragonal, Rhombohedral (or Trigonal), Hexagonal, and Cubic]. For all cubic systems the point group will always contain four 3-fold rotation axes through the corners.\n",
    "\n",
    "Each of these crystal systems may include one or several of the four centering types of Bravais lattices [Primitive (P), Base-Centered (C), Body-Centered (I), and Face-Centered (F)], which tells us which lattice points are centered in the *conventional* unit cell.\n",
    "\n",
    "The 7 crystal systems (symbol in end gives the possible centering types):\n",
    "* Cubic: ($\\alpha = \\beta = \\gamma = 90^\\circ, a = b = c $) {P, I, F}\n",
    "* Tetragonal: ($\\alpha = \\beta = \\gamma = 90^\\circ$, $a = b \\neq c$) {P, I}\n",
    "* Orthorhombic: ($\\alpha = \\beta = \\gamma = 90^\\circ$, $a \\neq b \\neq c$) {P, C, I, F}\n",
    "* Monoclinic: ($\\alpha = \\gamma = 90^\\circ, \\beta \\neq 90^\\circ$, $a \\neq b \\neq c$) {P, C}\n",
    "* Triclinic: ($\\alpha \\neq \\beta \\neq \\gamma \\neq 90^\\circ$, $a \\neq b \\neq c$) {P}\n",
    "* Hexagonal: ($\\alpha = \\beta = 90^\\circ, \\gamma = 120^\\circ, a = b \\neq c$) {P}\n",
    "* Trigonal: ($\\alpha = \\beta = \\gamma \\neq 90^\\circ$, $a = b = c$) {P}\n",
    "\n",
    "<div style=\"display:flex\">\n",
    "     <div style=\"flex:1;padding-right:10px;\">\n",
    "          <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/bravaislattice1-5.jpg?inline=false\" alt=\"graph\" width=350 >\n",
    "     </div>\n",
    "     <div style=\"flex:1;padding-center:10px;\">\n",
    "          <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/bravaislattice6-9.jpg?inline=false\" alt=\"graph\" width=500 >\n",
    "     </div>\n",
    "</div>\n",
    "<img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/bravaislattice10-14.jpg?inline=false\" alt=\"graph\" width=400 >\n",
    "<em> Figures from Physical Chemistry, 7th ed. (2002), by Atkins & de Paula </em>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 32 Crystallographic point groups and 230 space groups \n",
    "Real crystals are basically a combination of a Bravais lattice and a basis (unit or motif) of atoms or molecules which make up the unit cell. It is the unit cell that builds the crystal by infinite translations in units of the primitive lattice vectors, and it is the unit cell which is taken to itself using the symmetry operations which makes up the *crystallographic point groups* and the *crystallographic space groups*. The space groups are named or numbered according to any number of conventions. Two of the most common ones are the Hermann-Mauguin (H-M) system, and the number system (by International Union of Crystallography). In the captions under the figures below both are used for reference.\n",
    "\n",
    "There are in all 32 point groups (compared to the 7 lattice point groups) and 230 space groups. The extra groups are caused by two extra types of symmetry operations *glide plane and screw axis* that can be found in crystal unit cells. See for example the following links for online database of crystal spacegroups: http://webmineral.com/crystall.shtml and http://img.chem.ucl.ac.uk/sgp/mainmenu.htm (see caption under the figure above)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Conventional vs. Primitive Unit Cells\n",
    "The primitive unit cell is one that will only include a single Bravais lattice point. There are infinite many ways to construct such unit cells but the normal ways are by either making the parallelipiped of the primitive unit vectors, or by making a, so called, Wigner-Seitz cell. \n",
    "\n",
    "Copper, for example, has atoms in the middle of each of the sides as well as in each of the corners of a cube, and thus has a *Face-Centered Cubic* (FCC) conventional unit cell. Three different types of cubic unit cells are shown below together with their crystallographic space groups. The FCC unit cell to the far left also has the primitive lattice vectors added (in red).\n",
    "\n",
    "<div style=\"display:flex\">\n",
    "     <div style=\"flex:1;padding-right:20px;\">\n",
    "         <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/cu-unitcell.png?inline=false\" alt=\"graph\" width=180 >\n",
    "          <br><em>FCC unit cell (Fm$\\overline{3}$m, No. 225). Primitive vectors in red.</em>\n",
    "     </div>\n",
    "     <div style=\"flex:1;padding-center:20px;\">\n",
    "         <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/nacl-unitcell.png?inline=false\" alt=\"graph\" width=190 >\n",
    "         <br><em>FCC unit cell of NaCl (Fm$\\overline{3}$m, No. 225) </em>\n",
    "     </div>\n",
    "     <div style=\"flex:1;padding-left:20px;\">\n",
    "         <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/cscl-unitcell.png?inline=false\" alt=\"graph\" width=180 >\n",
    "         <br><em>BCC unit cell of CsCl (Pm3m, No. 221) </em>\n",
    "     </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Miller indices and Reciprocal Lattice\n",
    "In each crystal with primitive vectors $\\mathbf{a},\\mathbf{b}, \\mathbf{c}$ we can define the reciprocal lattice by the reciprocal primitive vectors:\n",
    "\n",
    "$\\mathbf{a^*} = (2\\pi/V_c)(\\mathbf{b}\\times \\mathbf{c})$\n",
    "\n",
    "$\\mathbf{b^*}  = (2\\pi/V_c)(\\mathbf{c}\\times \\mathbf{a})$\n",
    "\n",
    "$\\mathbf{c^*} = (2\\pi/V_c)(\\mathbf{a}\\times \\mathbf{b}),$\n",
    "\n",
    "where the volume of the primitive cell is: $V_c = \\mathbf{a^*}\\cdot(\\mathbf{b^*}\\times \\mathbf{c^*})$.\n",
    "\n",
    "For each vector $\\mathbf{G} = h\\mathbf{a^*} + k\\mathbf{b^*} + l\\mathbf{c^*}$ in the reciprocal space, there is a family of parallel planes denoted {$hkl$} which are orthogonal to $\\mathbf{G}$. So for example in a cubic unit cell with lattice constant, $c = b = a$ , the {100} planes of the FCC unit cell in the far left in figure above, are spanning the $\\mathbf{b}-\\mathbf{c}$ sides, orthogonal to the $\\mathbf{a}$ vector.\n",
    "\n",
    "What does this mean in practice? \n",
    "\n",
    "It means that in a diffractogram, the (100) hkl-peak (*if there is one*) will show up at an angle such that the scattering vector **Q** is equal to the vector (point) $1\\mathbf{a^*} + 0\\mathbf{b^*} + 0\\mathbf{c^*}$ in the reciprocal lattice (from the Laue condition), and represents the reflection of the beam off the {100}-family of planes in the crystal. As was shown in the example using the Cu-lattice in the figure above, **Q** is parallel with the **a**-vector in the crystal lattice, and the planes, in turn, are normal to the **a**-vector and are spanning the **b-c** sides."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Distance between planes\n",
    "Using the Laue or Bragg's conditions together with the definition of the lattice vectors for 7 crystal system gives:\n",
    "* Cubic lattice: $d_{hkl} = \\frac{a}{\\sqrt{(h^2 + k^2 + l^2)}}$\n",
    "* Orthorhombic lattice:$\\frac{1}{d^2_{hkl}} = \\frac{h^2}{a^2} + \\frac{k^2}{b^2} + \\frac{l^2}{c^2}$\n",
    "* Separation of {$nh,nk,nl$} planes are $n$ times smaller than the separation of the {$hkl$} planes. For example the distance between {220} planes are half the distance between {110}."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general you will have the following relationship:\n",
    "\n",
    "![d-spacing hkl](https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/dhkl.gif?inline=false)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1\n",
    "Calculate the separation of:\n",
    "\n",
    "a) the {123} planes and\n",
    "\n",
    "b) the {246} planes\n",
    "\n",
    "of an orthorhombic unit cell with a = 0.82 nm, b = 0.94, and c = 0.75 nm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "# import standard libraries \n",
    "\n",
    "%matplotlib inline\n",
    "import seaborn as sns\n",
    "sns.set(style='whitegrid')\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "d_123 = 0.21nm\n",
      "d_246 = 0.11nm\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "# Possible solution Task 1: Use the equation for orthorhombic unit cell:\n",
    "# 1/d² =a²/h² + b²/k² + c²/l² and substitute the values for (a,b,c) and (hkl).\n",
    "d123 = 1./(1./(0.82e-9)**2 + 2.**2/(0.94e-9)**2 + 3.**2/(0.75e-9)**2)\n",
    "print(f'd_123 = {round(np.sqrt(d123)/1e-9, 2)}nm')\n",
    "print(f'd_246 = {round(np.sqrt(d123)/2/1e-9, 2)}nm')\n",
    "#print(f'd_123 = {sqrt(d123)} m')\n",
    "#print(f'd_246 = {sqrt(d123)/2.}m')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2\n",
    "A first-order reflection from the {111} planes of a cubic crystal was observed at a glancing angle of $\\theta = 11.2^\\circ$ when Cu $K_\\alpha $X-rays of wavelength 154 pm were used. What is the length of the side of the unit cell?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "code_folding": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a = 686.6pm\n"
     ]
    }
   ],
   "source": [
    "# possible solution Task 2: For cubic unit cell we can combine Bragg's law lambda = 2dsin(theta)\n",
    "# with plane distance equation above d = a/sqrt(1² + 1² + 1²). d_111 = a/sqrt(3), solve of a:\n",
    "print(f'a = {np.round(np.sqrt(3)*154/(2*np.sin(11.2*2*np.pi/360)), 1)}pm')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 3\n",
    "An X-ray diffraction pattern of element polonium using 71 pm Mo X-rays gave the following peak positions of $2\\theta$ (in degrees). 12.1, 17.1, 21.0, 24.3, 27.2, 29.9, 34.7, 36.9, 38.9 40.9, 42.8.\n",
    "\n",
    "Identify the unit cell (cubic, orthorhombic, tetragonal...) and determine its dimensions (lattice constants). *Hint! Investigate the relative positions of $sin(2\\theta)$ for the peak, relative to the first peak, and check what that is suppose to be for cubic unitcell.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "code_folding": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sin^2(theta_hkl)/sin^2(theta_100):\n",
      "[1.0, 1.99, 2.99, 3.99, 4.98, 5.99, 8.01, 9.02, 9.98, 10.99, 11.99]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "from numpy import sin, pi\n",
    "\n",
    "# Task 3. Possible solution: Test if cubic first. For each hkl you get\n",
    "# sin(theta_hkl)² = (h² + k² + l²)*lambda/(2a) = constant*(h² + k² + l²)\n",
    "# sin²(theta_hkl)/sin²(theta_100) = 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12 if cubic.\n",
    "\n",
    "sinhkloversin = [round(sin(x*2*pi/360.)**2/sin(12.1*pi/360.)**2, 2) for x in 0.5*np.array([12.1, 17.1, 21.0, 24.3, 27.2, 29.9, 34.7, 36.9, 38.9, 40.9, 42.8])]\n",
    "print('sin^2(theta_hkl)/sin^2(theta_100):')\n",
    "print(sinhkloversin)\n",
    "\n",
    "# It seem to match with cubic pretty well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 4\n",
    "Make a python code that will \n",
    "\n",
    "a) print out a list of hkl values [[0,0,1], [0,0,2], [0,0,3], [0,1,1], [0,1,2],...]. (Hint! Use the itertool library and its combinations_with_replacements function).\n",
    "\n",
    "b) Make a python script that will take a wavelength, cubic unit-cell length *a*, and plot the $2\\theta$ values for a list of hkl values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "code_folding": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "List of hkl indices:\n",
      "[[0 0 1]\n",
      " [0 0 2]\n",
      " [0 0 3]\n",
      " [0 1 1]\n",
      " [0 1 2]\n",
      " [0 1 3]\n",
      " [0 2 2]\n",
      " [0 2 3]\n",
      " [0 3 3]\n",
      " [1 1 1]\n",
      " [1 1 2]\n",
      " [1 1 3]\n",
      " [1 2 2]\n",
      " [1 2 3]\n",
      " [1 3 3]\n",
      " [2 2 2]\n",
      " [2 2 3]\n",
      " [2 3 3]\n",
      " [3 3 3]]\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c05a080675a042d6b30eb429d3eb2ddc",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(IntSlider(value=300, description='a', max=400, min=200), Dropdown(description='l', optio…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Experimental 2theta:\n",
      "[12.1, 17.1, 21.0, 24.3, 27.2, 29.9, 34.7, 36.9, 38.9, 40.9, 42.8]\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAsUAAAFGCAYAAACCB+LzAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAlzklEQVR4nO3dbXQU5f3/8c+GkITEm1BMYhFFbUVuklAMYqQKVYlRTBATjlA8ILZGhSqFoxQRqPywCHhTpErFpFZbBYUiGNIqoHI86oFWSGtjgBSs2hY0u0QahJCE3Mz/AX+2xgAJkt1rZq/369HuzGSu7/Wd2fbT6TDjcxzHEQAAAGCxKNMFAAAAAKYRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsF60ycGbm5tVU1Ojzp07y+fzmSwFAAAAEcxxHDU0NCghIUFRUa2vCxsNxTU1Ndq5c6fJEgAAAGCRXr166fTTT2+13Ggo7ty5s6QjxcXExJgsRZJUXl6u1NRU02VEFHoa2T2IhLl5bQ5eq9dt3Ng/t9TkhjpM12ByfFNjm+55OB0+fFg7d+4M5s+vMxqKj94yERMTo9jYWJOlBLmljkhCTyO7B5EwN6/NwWv1uo0b++eWmtxQh+kaTI5vamzTPQ+3492yyz+0AwAAgPUIxQAAALCe0dsnTqS5uVm7d+9WTU1N2MaMjo7Wjh07wjae2yQkJKhHjx7H/BeZAAAAkcy1obiqqko+n08XX3xx2EJaTU2NEhISwjKW2zQ3N2vPnj2qqqpScnKy6XIAAADCyrWXBKurq5WSksJVyzCJiopSSkqK9u/fb7oUAACAsHNt4mxqajruIzMQGp07d1ZjY6PpMgAAAMKu3aH44MGDysnJ0e7du1ut27Fjh/Lz85Wdna2ZM2d2WLDiLXfhRb8BAICt2hWK//73v+uHP/yhPv3002OunzZtmmbPnq3169fLcRytXLmyI2uUJB1uaOrwfbZ3vwcOHNCkSZO0e/duXX311SGpAwAAAOa06x/arVy5Ug8++KB+9rOftVq3Z88e1dXV6Xvf+54kKS8vT7/61a80duzYDi00pnMn5d5b3KH7lKSSx29sc5v9+/eroqKiw8cGAACAO7QrFM+bN++46wKBgJKSkoLfk5KS5Pf7T70yF/nFL36hQCCg+fPnq66uTlOnTtWuXbt0xhlnaMmSJerataveeecd/epXv1JjY6N69Oihhx56SF27dtUHH3ygefPmqb6+Xl27dtXcuXPVs2dPjRs3TmeeeaZ27dql0aNHa9u2bXr88cclSU899ZRiYmJ0xx13GJ45AACAHU75kWyO47RadrL3ppaXl7daFh0d3eIZxaF8VFpNTY3i4roEx2hqalZdXW1w/b333quKigpNmTJFubm5GjNmjFJTUzVt2jStWbNG1157rR599FEVFhbqjDPO0KpVq7RgwQLNmDFDU6ZM0SOPPKJ+/frpjTfe0JQpU/Tiiy+qqalJF1xwgRYuXKhDhw6pqKhIe/fuVZcuXVRcXKyioqKwPqP5qMOHD6u0tPSU99O7Tz8lxMcpIyNDNYfqVLFjWwdU5y2R3IOvzk2SJ+fnxTn07tPPU/W6iRuPd2paf8XGRCsjI0P1hxtV/uHfjdXihnPLdA2mxjd1brrxN2HaKYfilJQUVVVVBb/v3bv3pJ9zm5qa2uq92zt27AjbM4OPjrPrP9WSpIvOTWwxdpcuXRQVFaUuXbooOTlZl112mSSpd+/eOnTokHbt2iW/36+77rpL0pFn/p555pkKBAJKTEzUoEGDJEkjR47UvHnz1NzcrE6dOmngwIFKSEhQQkKCfvCDH+i9997Tueeeq549e+r8888Py9y/LiYmRv379++QfR293aXk8RuDPzrbRHIPvno7k1fn58U5RPI5FWpuPN5uOp5uqMV0DabGN3VuuvE3EUr19fXHvBB71CmH4nPOOUexsbEqLS1VRkaGXn31VQ0ZMuRUd+ta0dH/a5nP55PjOGpqatIll1yipUuXSjrS9JqaGgUCgVZ/f3R7SYqLiwsuz8/P19NPP60ePXooLy8vxLMAAADAV33j5xQXFBToww8/lCQ99thjmj9/vq6//nrV1tZq/PjxHVagG0RHR5/wMXP9+/fXBx98oE8++USS9Otf/1qPPPKILrzwQlVXV6usrEyS9Nprr6l79+5KTExstY+BAweqsrJSf/nLXzRs2LCQzAMAAADHdlJXijdu3Bj8XFRUFPzcu3dvrVq1quOqcplu3bqpe/fumjFjxjHXJyUl6eGHH9aUKVPU3NyslJQUPfroo4qJidGiRYv00EMPqba2VmeeeaYWLVp03HGysrJUXV2tmJiYUE0FAAAAx3DKt0+Ey+GGpnY9Pu2b7Demc6cTbtO5c2e9/PLLrZbfc889wc9XX331MZ9hPGDAAP3hD39otfyFF14IfnYcRw0NDdqyZYseeOCBkykfAAAAHcC1r3n+uraCq9v2ezL27t2r73//++rfv7/69etnuhwAAADreOZKcSRLTk7Wli1bTJcBAABgLc9cKQYAAABChVAMAAAA6xGKAQAAYD1CMQAAAKxHKAYAAID1PBOK6+rqPLXfb2rx4sV66623Qrb/jRs36rnnngvZ/gEAALzIM49ki4uLk8/n6/D9Oo7T4fs8FT/96U9Duv9t27aFdP8AAABe5JlQbFphYaFef/11NTU16YorrtAll1yiRx55RCUlJaqsrNS4ceO0cuVKPfHEE/L5fNq5c6cOHjyoiRMnauTIkaqpqdHcuXO1a9cuNTU1qaCgQDk5OVq9erXWrFmj6upqXXXVVQoEAho0aJAGDRqkn/zkJzr33HO1c+dOpaamatCgQVqzZo3279+vJUuW6Dvf+Y7Kyso0f/581dXVqWvXrvq///s/nXvuuRo3bpzS0tJUWlqqffv2adasWTrnnHOCb+br3r278vPzDXcVAADAHQjF7fDOO++ovLxcq1atks/n07Rp01RTU6MBAwbo6aef1vvvv6/p06fr7LPPliT5/X69/PLL+uKLL5SXl6fvf//7+t3vfqd+/fpp4cKFOnjwoMaMGaP+/fsHt3/ttdcUHR2t+++/PzjuP/7xD82fP1+9e/dWdna2zjnnHK1YsUJPPfWUVqxYofvuu0+zZs3S0qVL1b17d7377ruaPXu2nn/+eUlSQ0ODVqxYoY0bN2rx4sVavXq1xowZI0kEYgAAgK8gFLfD5s2bVVZWpry8PElH7kPu3r27Zs6cqeHDh+uSSy7RDTfcENw+Ly9PnTt31tlnn61LLrlEpaWl2rRpk+rq6vTKK69Ikg4dOqRdu3ZJkvr27avo6NaH4qyzzlLfvn0lSWeffbYuv/xySUeu8u7evVuffvqp/vOf/2jixInBvzl48GDw85VXXilJuuiii1RdXd2BHQEAAIgshOJ2aGpq0q233qrbbrtNkvTll1+qU6dO8vv96tSpkz755BMdPnxYMTExkqROnToF/7a5uVnR0dFqbm7Wo48+qn79+kmSqqqqdOaZZ6qkpERxcXHHHPfo/o766n6P7rtHjx4qLi4O1llVVRVcHxsbK0khuRcbAAAgknjm6RMmZWZmqri4WDU1NWpsbNRPfvITvfbaa5oxY4ZmzpypSy+9VE888URw+9dff12O42jPnj0qKytTRkaGMjMz9dJLL0mSAoGARowYoc8///yU6rrwwgu1f/9+bd26VZL0yiuv6L777jvh33Tq1EmNjY2nNC4AAECk8cyV4rq6upA8KaKuru64V2qPuvrqq1VRUaGbb75ZTU1NuvLKK/Xf//5X3bp107XXXqvBgwcrJydH1157bXCf+fn5Onz4sObOnauuXbvq7rvv1pw5c5STk6OmpiZNmzZN5513XjDQfhMxMTFavHix5s2bp/r6ep122mlauHDhCf/m0ksv1fTp03XWWWdp3Lhx33hsAACASOKZUNxWcA31fidNmqRJkyYdc91pp52mt99+W5L08ssv67rrrgvef/zVbR577LFWf5uXl9di2wULFgQ/b9y4Mfj5hRdeOObfDBgwQKtWrWq1369u36NHj+C+Lr300hb7BQAAALdPAAAAAN65UuwVX73SCwAAAG/gSjEAAACs5+pQ7LZXMEc6+g0AAGzl2lAcFxenL774gqAWJo7j6IsvvgjZP2gEAABwM9feU9yjRw/t3r1be/fuDduYgf8ekiQ1HowP25huEhcXpx49epguAwAAIOxcG4o7d+6sCy64IKxj/uzeI2+GK3n8xrCOCwAAALNce/sEAAAAEC6EYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALAeoRgAAADWIxQDAADAeoRiAAAAWI9QDAAAAOsRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANYjFAMAAMB6hGIAAABYj1AMAAAA67UrFJeUlGj48OHKysrSsmXLWq3ftm2b8vPzNWLECN1555368ssvO7xQAAAAIFTaDMV+v1+LFi3S8uXLVVxcrBUrVuijjz5qsc28efM0efJkrV27VhdccIGeffbZkBUMAAAAdLQ2Q/GmTZuUmZmpxMRExcfHKzs7W+vWrWuxTXNzs2pqaiRJtbW1iouLC021AAAAQAi0GYoDgYCSkpKC35OTk+X3+1tsc//992vmzJm64oortGnTJo0ZM6bjKwUAAABCJLqtDRzHabXM5/MFP9fV1WnmzJn63e9+p/T0dD333HOaPn26CgsL211EeXl5u7cNlYyMjBbfS0tLDVUSGehnZPfg63OTvDc/L84hks+pUHPj8XbT8XRDLaZrMDW+qXPTjb8J09oMxSkpKdq6dWvweyAQUHJycvD7zp07FRsbq/T0dEnS6NGjtXjx4pMqIjU1VbGxsSf1N6F2rJMF3xz9jPweRML8vDYHr9XrNm7rn5vqcUMtpmswOb6psU33PNTq6+tPeCG2zdsnBg8erM2bN2vfvn2qra3Vhg0bNGTIkOD6nj17qrKyUh9//LEk6a233lJaWloHlA4AAACER7uuFE+dOlXjx49XQ0ODRo0apfT0dBUUFGjy5MlKS0vT/PnzNWXKFDmOo27duunhhx8OR+0AAABAh2gzFEtSbm6ucnNzWywrKioKfh46dKiGDh3asZUBAAAAYcIb7QAAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANYjFAMAAMB6hGIAAABYj1AMAAAA6xGKAQAAYD1CMQAAAKxHKAYAAID1CMUAAACwHqEYAAAA1iMUAwAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALAeoRgAAADWIxQDAADAeoRiAAAAWI9QDAAAAOsRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANYjFAMAAMB6hGIAAABYj1AMAAAA6xGKAQAAYD1CMQAAAKxHKAYAAID1CMUAAACwHqEYAAAA1iMUAwAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPXaFYpLSko0fPhwZWVladmyZa3Wf/zxxxo3bpxGjBihH//4x9q/f3+HFwoAAACESpuh2O/3a9GiRVq+fLmKi4u1YsUKffTRR8H1juNo4sSJKigo0Nq1a9WnTx8VFhaGtGgAAACgI7UZijdt2qTMzEwlJiYqPj5e2dnZWrduXXD9tm3bFB8fryFDhkiS7rrrLt1yyy2hqxgAAADoYNFtbRAIBJSUlBT8npycrLKysuD3f//73zrrrLM0ffp0bd++Xb169dLs2bNPqojy8vKT2j4UMjIyWnwvLS01VElkoJ+R3YOvz03y3vy8OIdIPqdCzY3H203H0w21mK7B1Pimzk03/iZMazMUO47TapnP5wt+bmxs1Pvvv68XX3xRaWlpeuKJJ7RgwQItWLCg3UWkpqYqNja23duHw7FOFnxz9DPyexAJ8/PaHLxWr9u4rX9uqscNtZiuweT4psY23fNQq6+vP+GF2DZvn0hJSVFVVVXweyAQUHJycvB7UlKSevbsqbS0NElSTk5OiyvJAAAAgNu1GYoHDx6szZs3a9++faqtrdWGDRuC9w9L0oABA7Rv3z5VVFRIkjZu3Kh+/fqFrmIAAACgg7V5+0RKSoqmTp2q8ePHq6GhQaNGjVJ6eroKCgo0efJkpaWlacmSJZo1a5Zqa2t19tln65FHHglH7QAAAECHaDMUS1Jubq5yc3NbLCsqKgp+7t+/v1atWtWxlQEAAABhwhvtAAAAYD1CMQAAAKxHKAYAAID1CMUAAACwHqEYAAAA1iMUAwAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALAeoRgAAADWIxQDAADAeoRiAAAAWI9QDAAAAOsRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsJ7PcRzH1OD19fUqLy9XamqqYmNjjdRQV1enuLi4k16HYztez2zqZSSfU5EwNy+eo5HQd5Pc2D+31OSG34PpXpgeX5Jy7y0Ofi55/MaQj2d6XFPayp3RBmpylbi4OPl8vmOuM/i/FzzreP20qZeRfE5Fwty8eI5GQt9NcmP/3FKTG34Ppntheny4B7dPAAAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALAeoRgAAADWIxQDAADAeoRiAAAAWI9QDAAAAOsRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANYjFAMAAMB6hGIAAABYj1AMAAAA6xGKAQAAYL12heKSkhINHz5cWVlZWrZs2XG3e/vtt3X11Vd3WHEAAABAOES3tYHf79eiRYu0evVqxcTEaMyYMbrsssv03e9+t8V2VVVVWrhwYcgKBQAAAEKlzSvFmzZtUmZmphITExUfH6/s7GytW7eu1XazZs3S3XffHZIiAQAAgFBqMxQHAgElJSUFvycnJ8vv97fY5ve//7369u2r/v37d3yFAAAAQIi1efuE4zitlvl8vuDnnTt3asOGDXr++edVWVn5jYooLy//Rn/XETIyMk64vrS0NEyVRIYT9dOWXkbyORUJc/PiORoJfTfJjf1zS01u+D2Y7oUbxw9H702N62ZthuKUlBRt3bo1+D0QCCg5OTn4fd26ddq7d6/y8/PV0NCgQCCgsWPHavny5e0uIjU1VbGxsSdZeni09WNB+9HLIyK5D16fm1fr92rdbuHG/rmhJjfUIJmvw8T4puZsutehVl9ff8ILsW3ePjF48GBt3rxZ+/btU21trTZs2KAhQ4YE10+ePFnr169XcXGxCgsLlZycfFKBGAAAADCtzVCckpKiqVOnavz48Ro5cqRycnKUnp6ugoICffjhh+GoEQAAAAipNm+fkKTc3Fzl5ua2WFZUVNRqux49emjjxo0dUxkAAAAQJrzRDgAAANYjFAMAAMB6hGIAAABYj1AMAAAA6xGKAQAAYD1CMQAAAKxHKAYAAID1CMUAAACwHqEYAAAA1iMUAwAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALAeoRgAAADWIxQDAADAeoRiAAAAWI9QDAAAAOsRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANYjFAMAAMB6hGIAAABYj1AMAAAA6xGKAQAAYD1CMQAAAKxHKAYAAID1CMUAAACwHqEYAAAA1iMUAwAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALAeoRgAAADWIxQDAADAeoRiAAAAWK9dobikpETDhw9XVlaWli1b1mr9m2++qRtvvFEjRozQpEmTtH///g4vFAAAAAiVNkOx3+/XokWLtHz5chUXF2vFihX66KOPgusPHjyoOXPmqLCwUGvXrtXFF1+sJ598MqRFAwAAAB2pzVC8adMmZWZmKjExUfHx8crOzta6deuC6xsaGjRnzhylpKRIki6++GJ9/vnnoasYAAAA6GBthuJAIKCkpKTg9+TkZPn9/uD3rl27atiwYZKkuro6FRYWBr8DAAAAXhDd1gaO47Ra5vP5Wi07cOCAJk2apN69e+umm246qSLKy8tPavuOlJGRccL1paWlYaokMpyon7b0MpLPqUiYmxfP0Ujou0lu7J9banLD78F0L9w4fjh6b2pcN2szFKekpGjr1q3B74FAQMnJyS22CQQC+vGPf6zMzEw98MADJ11EamqqYmNjT/rvwqGtHwvaj14eEcl98PrcvFq/V+t2Czf2zw01uaEGyXwdJsY3NWfTvQ61+vr6E16IbfP2icGDB2vz5s3at2+famtrtWHDBg0ZMiS4vqmpSXfddZeuv/56zZw585hXkQEAAAA3a9eV4qlTp2r8+PFqaGjQqFGjlJ6eroKCAk2ePFmVlZXavn27mpqatH79eklHrvzOmzcv5MUDAAAAHaHNUCxJubm5ys3NbbGsqKhIkpSWlqaKioqOrwwAAAAIE95oBwAAAOsRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANYjFAMAAMB6hGIAAABYj1AMAAAA6xGKAQAAYD1CMQAAAKxHKAYAAID1CMUAAACwHqEYAAAA1iMUAwAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALAeoRgAAADWIxQDAADAeoRiAAAAWI9QDAAAAOsRigEAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANYjFAMAAMB6hGIAAABYj1AMAAAA6xGKAQAAYD1CMQAAAKxHKAYAAID1CMUAAACwHqEYAAAA1iMUAwAAwHqEYgAAAFiPUAwAAADrEYoBAABgPUIxAAAArNeuUFxSUqLhw4crKytLy5Yta7V+x44dys/PV3Z2tmbOnKnGxsYOLxQAAAAIlTZDsd/v16JFi7R8+XIVFxdrxYoV+uijj1psM23aNM2ePVvr16+X4zhauXJlyAoGAAAAOlqboXjTpk3KzMxUYmKi4uPjlZ2drXXr1gXX79mzR3V1dfre974nScrLy2uxHgAAAHC76LY2CAQCSkpKCn5PTk5WWVnZcdcnJSXJ7/e3a3DHcSRJhw8fbnfBofDtb3/7mMvr6+vDXElkOFY/betlJJ9TkTA3L56jkdB3k9zYP7fU5Ibfg+lemB4/MaFT2Mc0Oa4pR/Pm0fz5dT7neGv+v6VLl6q2tlZTp06VJP3hD3/Qhx9+qLlz50qS/vrXv+rRRx/VSy+9JEn617/+pTvvvLNdV4sPHDignTt3tn82AAAAwCno1auXTj/99FbL27xSnJKSoq1btwa/BwIBJScnt1hfVVUV/L53794W608kISFBvXr1UufOneXz+dr1NwAAAMDJchxHDQ0NSkhIOOb6NkPx4MGD9eSTT2rfvn3q0qWLNmzYoIceeii4/pxzzlFsbKxKS0uVkZGhV199VUOGDGlXcVFRUcdM6gAAAEBHi4uLO+66Nm+fkI48ku2ZZ55RQ0ODRo0apYKCAhUUFGjy5MlKS0tTRUWFZs2apZqaGvXt21fz589XTExMh04CAAAACJV2hWIAAAAgkvFGOwAAAFiPUAwAAADrEYoBAABgPUIxAAAArEcoBgAAgPUIxQAAALBemy/viETXXHPNCdc7jqOoqCi9+eabYarI++hpZPcgEubmtTl4rV63cWP/+vbt267ttm/fHtI63NAb0zWYHN/U2KZ77gVWhuK4uDgVFhYed73jOLrzzjvDWJH30dPI7kEkzM1rc/BavW7jxv516tRJzz///HHXO46j2267LeR1uKE3pmswOb6psU333BMcC23ZsqVDtsH/0NPI7kEkzM1rc/BavW7jxv69+OKLHbLNqXJDb0zXYHJ8U2Ob7rkXWBmKAZycPXv2OBMnTnRuuukmZ8mSJU5jY2Nw3R133GGwsvbz2hy8Vq/buLF/f/3rX53Bgwc76enpzq233urU19cH12VmZoatji+//NL55S9/6Tz77LNOZWWlM3r0aGfAgAHO7bff7lRWVlpRg8nzw9TcTffcC6x8zfNTTz11wvV33313mCqJHPQ0sntw2223KScnRxdffLGeeuopNTU16emnn1Z0dLRGjhypV1991XSJbfLaHLxWr9u4sX+DBg3SsGHDdNlll2nhwoVqbm7W22+/rbi4OKWnp6usrCwsdUyaNEnf+c535Pf79f7772vixIkaMWKEXnvtNb3xxhtaunRpxNdg8vwwNXfTPfcCK58+0dTUpGeffVbNzc2mS4kY9DSye1BdXa38/Hylpqbq6aef1umnn65p06aZLuukeG0OXqvXbdzYv/r6ej388MO68cYb9d5776lLly667rrrwl7H7t27de+992revHlqaGjQ6NGj1aVLF+Xn58vv91tRg8nzw9TcTffcC6z8h3Y//elPFQgE1KVLFxUUFJguJyLQ08juQadOnbRr1y5ddNFF8vl8WrhwoW6//Xb9/Oc/V1NTk+ny2sVrc/BavW7jxv75fD69+eabGjZsmKKiovSnP/1JQ4cO1fDhwxXO/9M2OjpaH3/8sS688EI999xzweXbt2+Xz+ezogaT54epuZvuuScYvn3DmAMHDjhr1qwxXUZEoaeR24OtW7c6V111lbN27drgspqaGmfixIlO7969DVbWfl6bg9fqdRs39m/ZsmVOnz59nDlz5gSXVVVVOYMHD3Z69eoVtjq2bNniXHvttS3uo33jjTecK6+80iktLbWiBpPnh6m5m+65F1h5TzGAb+bw4cOKiYlpsWzHjh3q06ePoYpOntfm4LV63caN/aupqVFCQkKLZX/60590ww03GKroSJ+io6MVFWXurkoTNbjl/DDVfzccdzchFAMAAMB6/E8DAAAAWI9QDAAAAOtZGYqbm5v18ssv69Zbb9V1112n4cOHa8KECfrtb3+rhoYG0+UBrsNvBl5z4MABzZs3T/fcc4+Ki4tbrJs9e7aRmj7//HONHDlSV155pR588MEW666//nojNdnK5Plhamw3/ibcxspHsj344INqbm7WPffco+TkZElSIBBQcXGxZsyYoccee8xwhd7T1oPOR44cGZY6TIrkHkTCb8Zrx8dr9brNjBkz1KtXLw0cOFCFhYXaunWrHnroIUlSeXm5kZpuvvlmnXfeebr88sv10ksv6f3339frr78uSfrss8/CVocbzi3TNZg8P0yN7cbfhNtYGYq3bNmidevWtVh23nnnaeDAgUb/9a+X/fnPf9b69euP+yB6G/4LPJJ7EAm/Ga8dH6/V6za7d+8OvmVy6NChuuOOO7RgwQLdf//9YX0m8Fft379fy5YtkyTdeeedysrKUl5enlavXh3WmtxwbpmuweT5YWpsN/4m3MbKUHzaaaeprKxM6enpLZb/7W9/U3x8vKGqvG3BggWqrq5WRkaGRo0aZbocIyK5B5Hwm/Ha8fFavW60d+9eJSUlKS4uTkuWLNEtt9yipUuXGn1RQUVFhXr37q3ExEStXbtW2dnZ+tGPfhTWmtxwbrmhBpPnh6mx3fibcBVjT0g2aPv27U5OTo6TlZXljB071hk7dqyTlZXl5OTkOBUVFabL8yy/3+/85je/MV2GUZHag0j5zXjt+HitXjd54403nCuuuMJ58803g8sCgYBz0003OX369DFS02OPPeb07t3b+eUvfxlctmPHDic9PT2sL+9wHHecWyZrMHl+mBrbjb8Jt7H6OcWfffaZAoGAHMdRSkqKunfvbrokwNX4zcBLDh48qMbGRiUmJgaXNTc3a+PGjRo2bJiRmvx+v+rq6tSzZ8/gssbGRi1evFj33nuvkZpsZfL8MDW2G38TbmJ1KAYAAAAkSx/JBgAAAHwVoRgAAADWs/LpE5L07rvvat26daqsrFRUVJSSk5M1ZMgQZWdnmy7Ns+hpZIuE4xsJc0D7cbyPz/beNDY2atmyZfr88881bNgwDRw4MLjuySef1D333BPS8W3vv1tZeU/x4sWLVVZWphEjRrR4EcEf//hHffe739X06dMNV+g99PTIs3xP5NJLLw1TJR0vEo6v1+YQyedTOLjxeL/wwgsnXD9u3Liw1OGG3pg+vx944AE1NzerV69eevHFF3XzzTfrrrvukiTddNNNWrNmTcjGNtV/0z33AitDcXZ2tl5//XVFRbW8e6SpqUk5OTnBNwyh/eipNGHCBH3wwQdKT09v9SB0n8+n3//+94YqO3WRcHy9NodIPp/CwY3He9CgQdq/f7/OOOOMYx7TtkJLR3FDb0yf3yNGjNDatWslSfv27dOECROUl5enCRMmaOTIkW2+ce9UmOq/6Z57gZW3T8TGxqqysrLV46Q+++wzxcTEGKrK2+ipVFRUpPHjx+vWW2/VNddcY7qcDhUJx9drc4jk8ykc3Hi83377bV1xxRUaO3aspk6daqQGyR29MX1+O46jQ4cOKT4+Xt/61rdUVFSkH/7wh+rWrVvIX2Rhqv+me+4FVl4p3rRpk2bOnKnzzz9fSUlJko685eXTTz/V/PnzlZmZabhC76GnR3zyySd65ZVXdN9995kupUNFwvH14hwi9XwKB7ce73fffVdPPvmkVq5caWR8yT29MXl+r1ixQr/97W81Z84cXX755ZKkf/7zn7r99tv1xRdfqKysLGRjm+w//5lyYlaGYkmqr69XWVlZixcR9O/f35VXjLyCnka2SDi+kTAHtB/H+/jojfTpp58qJiamxRXbgwcPatWqVZowYUJIx6b/LhWmN+e5ym233dYh2+B/6Glk9yAS5ua1OXitXrdxY/8GDRrUIducKjf0xnQNJsc3NbbpnnuBlfcU/+1vf9P48eOPu95xHG3bti2MFXkfPY3sHkTC3Lw2B6/V6zZu7F91dfUJ/4W/4zg6cOBAyOtwQ29M12ByfFNjm+65F1gZip955hnTJUQcehrZPYiEuXltDl6r123c2L8HHnjAdAmS3NEb0zWYHN/U2KZ77gXW3lMMAAAAHMVrngEAAGA9QjEAAACsRygGAACA9QjFAAAAsB6hGAAAANb7f8zRPae6OTfHAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 864x360 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# cubic unit cell h k l lines\n",
    "\n",
    "import numpy as np\n",
    "import itertools as it\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "# make a list of {h k l} combinations [[0 0 1], [0 0 2],...[3 3 3]]\n",
    "# use itertool to make this much easier. \n",
    "# https://docs.python.org/3/library/itertools.html#module-itertools\n",
    "# I make it into an array to be able to use vector math on it. \n",
    "# list() will take the \n",
    "hkl = np.array(list(it.combinations_with_replacement(range(4), 3)))[1:]\n",
    "\n",
    "# This is answer to problem a)\n",
    "print('List of hkl indices:')\n",
    "print(hkl)\n",
    "\n",
    "# use interact decorator to manually change value on a\n",
    "@interact\n",
    "def twotheta_cubic(a = (200,400,1), l=[71.]):\n",
    "    '''function that computes the hkl positions of a\n",
    "    cubic unit-cell for given array of {h k l} indices, \n",
    "    lattice constant, a, in pm, and wavelength in pm'''\n",
    "    sintheta = np.linalg.norm(hkl, axis=1)*l/(2*a)\n",
    "    hkltheta = np.arcsin(sintheta)*360./(2.*pi)\n",
    "    \n",
    "    return 2*hkltheta\n",
    "\n",
    "\n",
    "theta = np.linspace(0, 200, 401)\n",
    "\n",
    "# values of 2theta from experiments\n",
    "thetaexperiment = [12.1, 17.1, 21.0, 24.3, 27.2, 29.9, 34.7, 36.9, 38.9, 40.9, 42.8]\n",
    "\n",
    "# This is answer to problem b)\n",
    "print('Experimental 2theta:')\n",
    "print(thetaexperiment)\n",
    "\n",
    "# Here i just play a bit \n",
    "# make a plot of experiment and theoretical h k l 2*theta\n",
    "plt.figure(figsize=(12,5))\n",
    "# make Intensity = 0.5 for all experimental 2theta.\n",
    "iexperiment=0.5*np.ones(np.shape(thetaexperiment))\n",
    "\n",
    "# make an array of 2theta by calling function\n",
    "hkltheta2 = twotheta_cubic(a=337, l=71.)\n",
    "\n",
    "intensity = np.ones(np.shape(hkltheta2))\n",
    "\n",
    "#plot a bargraph\n",
    "plt.bar(hkltheta2, intensity, width=0.2)\n",
    "plt.bar(thetaexperiment, iexperiment, color='black', width=0.4)\n",
    "\n",
    "# use hkl indices as xticks\n",
    "labels = [str(hklpoint) for hklpoint in hkl]\n",
    "plt.xticks(hkltheta2, tuple(labels), rotation='vertical')\n",
    "plt.xlim([10, 50])\n",
    "plt.legend(['theory', 'experiment'])\n",
    "plt.show()\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
