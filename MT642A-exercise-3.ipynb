{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MT642A 2022, Exercise 3, Elastic Scattering and Diffraction 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Theory and Concepts\n",
    "<img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/scatteringvector.png?inline=false\" alt=\"drawing\" width=\"600\"/>\n",
    "\n",
    "------\n",
    "#### Scattering of wave of particle or oscillating electro-magnetic field\n",
    "Particles, like a neutrons or electrons as well as electromagnetic (EM) radiation, traveling with momentum $\\mathbf{p} = m\\mathbf{v}$ may all be treated as a plane wave with wave vector $\\mathbf{k} = \\hbar \\mathbf{p}$, propagating in space and time so that its amplitude vary according to:\n",
    "$$\\psi(\\mathbf{r}, t) = Ae^{i(\\mathbf{k}\\cdot\\mathbf{r} - \\omega t + \\phi_0)},$$\n",
    "where $\\phi_0$ is the phase offset at origin. The length of the wave vector $|\\mathbf{k}|$ is called the wavenumber $k$ and from the de Broglie relation one has: $k = 2\\pi/\\lambda$.\n",
    "\n",
    "For a single scattering event (inelastic or elastic) the incoming EM field or particle interacts with the scattering point (e.g. an electron through the polarizing effect of an oscillating electric field, or a nucleus via the strong force between neutron and proton). This will result in a change in momentum for the particle (or EM wave) so that the scattered wave have a new wave vector $\\mathbf{k'}$. $\\mathbf{Q} = \\mathbf{k} - \\mathbf{k'}$ is called the scattering vector (if one defines $\\mathbf{Q} = \\mathbf{k'} - \\mathbf{k}$ instead then the scattering vector will just point away from scatter point instead of towards it) and by using the cosine rule in the left figure it is easy to see that the following is true:\n",
    "$$Q^2 = k^2 + k'^2 - 2kk'\\cos(2\\theta).$$\n",
    "For the case of elastic scattering there is no energy transfer and so $k' = k$, and $Q = 2k \\sin(\\theta).$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Bragg's law\n",
    "If the incident plane wave scatters from multiple objects, the amplitudes of the scattered waves will interfere and add to each other. Let's say that there are two scatterers, *i* and *j*.\n",
    "The difference of the phases, $\\Delta\\Phi$, of the scattered waves is equal to $2d\\sin(\\theta)$, where $d$ is the distance between the scattering objects. The waves will therefore interfere constructively only if $2d\\sin(\\theta) = n\\lambda$, where $n \\in \\{1, 2, ...\\}$; this is called the Bragg's law, or the Bragg's condition for constructive interference.\n",
    "\n",
    "As $k = 2\\pi/\\lambda $, one can see that the Bragg's condition is fulfilled if $$2d\\sin(\\theta) = 2\\pi n/k \\iff 2k\\sin(\\theta) = 2n\\pi/d \\iff Q = 2\\pi/d$$\n",
    "\n",
    "This is very close to how we define the reciprocal lattice $\\mathbf{G}$, see below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Laue condition for constructive interference\n",
    "The Bragg's law is very much a combination of trigomentry and superposition principle of waves.\n",
    "More generally, the scattering amplitude is given as a Fourier transform (or convolution) of the unit cell structure factor and Lattice sum: \n",
    "$F^{crystal} = \\sum_j f_j(\\mathbf{Q})e^{i\\mathbf{Q}\\cdot\\mathbf{r_j}} \\sum_n e^{i\\mathbf{Q}\\cdot\\mathbf{R_n}}$, \n",
    "where $\\mathbf{r_j}$ is the position of atom-*j*, inside the unit cell (the basis), $f_j(\\mathbf{Q}) = \\int \\rho(\\mathbf{r})e^{i\\mathbf{Q}\\cdot\\mathbf{r}}$ are the (x-ray) atomic form factor for atom-*j*, ( scattering lengths $B_j$ for neutrons), $\\mathbf{R_n}$ are the lattice vectors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Lattice sum is basically just phase-factors ranging between -1 and 1 on the unit circle, and as such the sum will basically be of the order of unity for all $\\mathbf{Q}$ except for those where $\\mathbf{Q}\\cdot\\mathbf{R_n} = 2\\pi m;~ m=\\{1,2,3\\dots\\}~~~~~~ $     (A).\n",
    "\n",
    "Further down we will define the reciprocal lattice, $\\mathbf{G}$ and the nature of that is that the scalar product between $\\mathbf{G}$ and any lattice vector $\\mathbf{R_n}$ is equal to $2\\pi m$ where *m* is an integer. It can then be seen that relationship (A) above is true if and only if $\\mathbf{Q} =\\mathbf{G}$.\n",
    "\n",
    "This is the Laue condition for constructive scattering.\n",
    "\n",
    "Finally the intensity of the experimental peaks is proportional to the squared modulus of the scattering amplitude:\n",
    "\n",
    "$I \\propto |F(Q)|^2 $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Crystal Bravais Lattice\n",
    "In a crystal, objects like atoms, ions, polymers, or whole proteins, occupy symmetric points, or lattice points, situated on, or inside, a 3-dimensional infinite array called a *Bravais* lattice. The Bravais lattice is the collection of points $\\mathbf{x_i}$ in space generated by integer linear combinations of its three *primitive* lattice vectors {$\\mathbf{a}, \\mathbf{b}, \\mathbf{c}$}, through: $\\mathbf{x} = n_1\\mathbf{a} + n_2\\mathbf{b} + n_3\\mathbf{c};\\, {n_1, n_2, n_3} \\in \\{0, 1, 2,\\dots\\}$. A consequence of the lattice is generated from primitive lattice vectors, is that there is only a single lattice point inside the unit cell."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 14 Space groups and 7 Point groups\n",
    "There exist symmetry operations such as *translations, rotations, inversions, and mirror planes* that each one or a combination of will overlap the new lattice on-top of the old lattice. A group of all such combinations for a single Bravais lattice is called a *space group*. For symmetry reasons there are only 14 unique such space groups (and Bravais lattices). A sub-group of a space group is the collection of only those symmetry operations that will leave at least one lattice point unchanged while still take the lattice into itself. This collection is called a *point group*. It turns out that there are 7 unique point groups and they make up the 7 crystal systems or families [Triclinic, Monoclinic, Orthorhombic, Tetragonal, Rhombohedral (or Trigonal), Hexagonal, and Cubic]. For all cubic systems the point group will always contain four 3-fold rotation axes through the corners.\n",
    "\n",
    "Each of these crystal systems may include one or several of the four centering types of Bravais lattices [Primitive (P), Base-Centered (C), Body-Centered (I), and Face-Centered (F)], which tells us which lattice points are centered in the *conventional* unit cell.\n",
    "\n",
    "The 7 crystal systems (symbol in end gives the possible centering types):\n",
    "* Cubic: ($\\alpha = \\beta = \\gamma = 90^\\circ, a = b = c $) {P, I, F}\n",
    "* Tetragonal: ($\\alpha = \\beta = \\gamma = 90^\\circ$, $a = b \\neq c$) {P, I}\n",
    "* Orthorhombic: ($\\alpha = \\beta = \\gamma = 90^\\circ$, $a \\neq b \\neq c$) {P, C, I, F}\n",
    "* Monoclinic: ($\\alpha = \\gamma = 90^\\circ, \\beta \\neq 90^\\circ$, $a \\neq b \\neq c$) {P, C}\n",
    "* Triclinic: ($\\alpha \\neq \\beta \\neq \\gamma \\neq 90^\\circ$, $a \\neq b \\neq c$) {P}\n",
    "* Hexagonal: ($\\alpha = \\beta = 90^\\circ, \\gamma = 120^\\circ, a = b \\neq c$) {P}\n",
    "* Trigonal: ($\\alpha = \\beta = \\gamma \\neq 90^\\circ$, $a = b = c$) {P}\n",
    "\n",
    "<div style=\"display:flex\">\n",
    "     <div style=\"flex:1;padding-right:10px;\">\n",
    "          <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/bravaislattice1-5.jpg?inline=false\" alt=\"graph\" width=350 >\n",
    "     </div>\n",
    "     <div style=\"flex:1;padding-center:10px;\">\n",
    "          <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/bravaislattice6-9.jpg?inline=false\" alt=\"graph\" width=500 >\n",
    "     </div>\n",
    "</div>\n",
    "<img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/bravaislattice10-14.jpg?inline=false\" alt=\"graph\" width=400 >\n",
    "<em> Figures from Physical Chemistry, 7th ed. (2002), by Atkins & de Paula </em>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 32 Crystallographic point groups and 230 space groups \n",
    "Real crystals are basically a combination of a Bravais lattice and a basis (unit or motif) of atoms or molecules which make up the unit cell. It is the unit cell that builds the crystal by infinite translations in units of the primitive lattice vectors, and it is the unit cell which is taken to itself using the symmetry operations which makes up the *crystallographic point groups* and the *crystallographic space groups*. The space groups are named or numbered according to any number of conventions. Two of the most common ones are the Hermann-Mauguin (H-M) system, and the number system (by International Union of Crystallography). In the captions under the figures below both are used for reference.\n",
    "\n",
    "There are in all 32 point groups (compared to the 7 lattice point groups) and 230 space groups. The extra groups are caused by two extra types of symmetry operations *glide plane and screw axis* that can be found in crystal unit cells. See for example the following links for online database of crystal spacegroups: http://webmineral.com/crystall.shtml and http://img.chem.ucl.ac.uk/sgp/mainmenu.htm (see caption under the figure above)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Conventional vs. Primitive Unit Cells\n",
    "The primitive unit cell is one that will only include a single Bravais lattice point. There are infinite many ways to construct such unit cells but the normal ways are by either making the parallelipiped of the primitive unit vectors, or by making a, so called, [Wigner-Seitz cell](https://en.wikipedia.org/wiki/Wigner%E2%80%93Seitz_cell). \n",
    "\n",
    "Copper, for example, has atoms in the middle of each of the sides as well as in each of the corners of a cube, and thus has a *Face-Centered Cubic* (FCC) conventional unit cell. Three different types of cubic unit cells are shown below together with their crystallographic space groups. The FCC unit cell to the far left also has the primitive lattice vectors added (in red).\n",
    "\n",
    "<div style=\"display:flex\">\n",
    "     <div style=\"flex:1;padding-right:20px;\">\n",
    "         <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/cu-unitcell.png?inline=false\" alt=\"graph\" width=180 >\n",
    "          <br><em>FCC unit cell (Fm$\\overline{3}$m, No. 225). Primitive vectors in red.</em>\n",
    "     </div>\n",
    "     <div style=\"flex:1;padding-center:20px;\">\n",
    "         <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/nacl-unitcell.png?inline=false\" alt=\"graph\" width=190 >\n",
    "         <br><em>FCC unit cell of NaCl (Fm$\\overline{3}$m, No. 225) </em>\n",
    "     </div>\n",
    "     <div style=\"flex:1;padding-left:20px;\">\n",
    "         <img src=\"https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/cscl-unitcell.png?inline=false\" alt=\"graph\" width=180 >\n",
    "         <br><em>BCC unit cell of CsCl (Pm3m, No. 221) </em>\n",
    "     </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Miller indices and Reciprocal Lattice\n",
    "In each crystal with primitive vectors $\\mathbf{a},\\mathbf{b}, \\mathbf{c}$ we can define the reciprocal lattice by the reciprocal primitive vectors:\n",
    "\n",
    "* $\\mathbf{a^*} = (2\\pi/V_c)(\\mathbf{b}\\times \\mathbf{c})$\n",
    "* $\\mathbf{b^*}  = (2\\pi/V_c)(\\mathbf{c}\\times \\mathbf{a})$\n",
    "* $\\mathbf{c^*} = (2\\pi/V_c)(\\mathbf{a}\\times \\mathbf{b}),$\n",
    "\n",
    "where the volume of the primitive cell is: $V_c = \\mathbf{a}\\cdot(\\mathbf{b}\\times \\mathbf{c})$.\n",
    "\n",
    "For each vector $\\mathbf{G} = h\\mathbf{a^*} + k\\mathbf{b^*} + l\\mathbf{c^*}$ in the reciprocal space, there is a family of parallel planes denoted {$hkl$} which are orthogonal to $\\mathbf{G}$. So for example in a cubic unit cell with lattice constant, $c = b = a$ , the {100} planes of the FCC unit cell in the far left in figure above, are spanning the $\\mathbf{b}-\\mathbf{c}$ sides, orthogonal to the $\\mathbf{a}$ vector.\n",
    "\n",
    "What does this mean in practice? \n",
    "\n",
    "It means that in a diffractogram, the (100) hkl-peak (*if there is one*) will show up at an angle such that the scattering vector **Q** is equal to the vector (point) $1\\mathbf{a^*} + 0\\mathbf{b^*} + 0\\mathbf{c^*}$ in the reciprocal lattice (from the Laue condition), and represents the reflection of the beam off the {100}-family of planes in the crystal. As was shown in the example using the Cu-lattice in the figure above, **Q** is parallel with the **a**-vector in the crystal lattice, and the planes, in turn, are normal to the **a**-vector and are spanning the **b-c** sides."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Distance between planes\n",
    "* Laue conditions: For scattering of {hkl} planes in a crystal there will be diffraction only if $\\mathbf{Q} = \\mathbf{G} = h\\mathbf{a^*} + k\\mathbf{b^*} + l\\mathbf{c^*}$\n",
    "* Cubic lattice: $d_{hkl} = \\frac{a}{\\sqrt{(h^2 + k^2 + l^2)}}$\n",
    "* Orthorhombic lattice: $\\frac{1}{d^2_{hkl}} = \\frac{h^2}{a^2} + \\frac{k^2}{b^2} + \\frac{l^2}{c^2}$\n",
    "* Separation of {$nh,nk,nl$} planes are $n$ times smaller than the separation of the {$hkl$} planes. For example the distance between {220} planes are half the distance between {110}."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general you will have the following relationship:\n",
    "\n",
    "![d-spacing hkl](https://gitlab.com/knjakob-blomquist/mt642a/-/raw/main/dhkl.gif?inline=false)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1\n",
    "Calculate the separation of:\n",
    "\n",
    "a) the {123} planes and\n",
    "\n",
    "b) the {246} planes\n",
    "\n",
    "of an orthorhombic unit cell with a = 0.82 nm, b = 0.94, and c = 0.75 nm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2\n",
    "A first-order reflection from the {111} planes of a cubic crystal was observed at a glancing angle of $\\theta = 11.2^\\circ$ when Cu $K_\\alpha $X-rays of wavelength 154 pm were used. What is the length of the side of the unit cell?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 3\n",
    "An X-ray diffraction pattern of element polonium using 71 pm Mo X-rays gave the following peak positions of $2\\theta$ (in degrees). 12.1, 17.1, 21.0, 24.3, 27.2, 29.9, 34.7, 36.9, 38.9 40.9, 42.8.\n",
    "\n",
    "Identify the unit cell (cubic, orthorhombic, tetragonal...) and determine its dimensions (lattice constants). (*Hint! Can you find an expression for $sin^2(\\theta_{hkl})/sin^2(\\theta_{100}) $?)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 4 (extra)\n",
    "Make a python code that will \n",
    "\n",
    "a) print out a list of hkl values [[0,0,1], [0,0,2], [0,0,3], [0,1,1], [0,1,2],...]. (Hint! Use the itertool library and its combinations_with_replacements function).\n",
    "\n",
    "b) Make a python script that will take a wavelength, cubic unit-cell length *a*, and plot the $2\\theta$ values for a list of hkl values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
